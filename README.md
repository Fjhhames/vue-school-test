
# Vue School Test

## Deployment 
The following steps will get the application working locally for test.


### Installing

Clone the repository and install dependencies
```
git clone https://Fjhhames@bitbucket.org/Fjhhames/vue-school-test.git
cd vue-school-test
composer install
```


### Migrate db and Seed users

Before this, setup your database and add credentials to environment

Migrate tables and seed user

```
php artisan migrate --seed
```

### Test out feature

To test our API calls. Run the task scheduler command

```
while true; do php artisan schedule:run; sleep 60; done
```

### Update a user
The API calls (or logs in this scenerio) only happens for updated users. 
to update a user 

```
php artisan user:update {email}
```

{email} here is the email of the user you wish to update.

### Check logs
The Scheduler Logs are found in the file *storage/logs/schedulerOutput.log*

### Thanks!
This was a really good test. I enjoyed every minute coding this. 