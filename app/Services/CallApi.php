<?php
namespace App\Services;

use App\Batch;
use Illuminate\Support\Facades\Log;
use Symfony\Component\Console\Output\ConsoleOutput;

class CallApi
{

    protected $batches;

    public function __invoke(): void
    {
        $this->batches = Batch::all()->take(1000);
        
        if(count($this->batches) > 0){
            $this->sendApi();
        }else{
            $log = "All attributes up to date with provider";
            
            $this->log($log);
            Log::channel('scheduler')->info($log);
        }
    }

    public function sendApi()
    {
        $data = $this->getApiData(); // if this is sent to an external api it'll be json encoded.

        foreach($data['batches']['subscribers'] as $user){
            $log = "[$user->id] firstname: $user->firstname, lastname: $user->lastname, timezone: $user->timezone";
            Batch::where('user', $user->email)->delete();

            $this->log($log);
            Log::channel('scheduler')->info($log);
        }
    }

    public function getUpdatedUsers(): array
    {
        $updatedUsers = [];
        foreach ($this->batches as $batch) {
            array_push($updatedUsers, $batch->getUser());
        }

        return $updatedUsers;
    }

    public function getApiData(): array
    {
        return [
            'batches' => [
                'subscribers' => $this->getUpdatedUsers(),
            ],
        ];
    }

    public function log(String $text): void
    {
        $output = new ConsoleOutput();

        $output->writeln("<info>$text</info>");
    }
}