<?php

namespace App\Console\Commands;

use App\User;
use Faker\Factory;
use App\Events\UserUpdated;
use Illuminate\Console\Command;


class UpadateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:update {user}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Randomly updates user\'s firstname, lastname and timezone';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
       $this->updateUser();
    }

    public function updateUser(){
        $faker = Factory::create();
        $timezones = ["CET", "CST", "GMT+1"];

        try {
            $user = User::where('email', $this->argument('user'))->firstOrFail();
            $user->firstname = $faker->firstname;
            $user->lastname = $faker->lastname;
            $user->timezone = $timezones[array_rand($timezones)];

            $user->save();
        } catch (\Exception $e) {
            $error = $e->getMessage();
        }

        if(isset($error)){
            $this->error($error);
        }else{
            $this->info('User updated');
        }
        
    }
}
