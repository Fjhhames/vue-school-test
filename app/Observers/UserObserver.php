<?php

namespace App\Observers;

use App\User;
use App\Batch;
use Symfony\Component\Console\Output\ConsoleOutput;

class UserObserver
{
    /**
     * Handle the user "updated" event.
     *
     * @param  \App\User  $user
     * @return void
     */
    public function updated(User $user)
    {
        $batch = Batch::firstOrCreate([
            'user' => $user->email
        ]);

        $output = new ConsoleOutput();
        $output->writeln("<info>$batch->user added to API batch<info>");
    }

}
