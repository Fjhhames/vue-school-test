<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Batch extends Model
{
    protected $fillable =  ['user'];

    public function scopeGetUser(){
        return User::where('email', $this->user)->firstOrFail();
    }
}
